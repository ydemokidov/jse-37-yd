package com.t1.yd.tm.exception.system;

import org.jetbrains.annotations.NotNull;

public class ArgumentNotSupportedException extends AbstractSystemException {

    public ArgumentNotSupportedException() {
        super("Error! Argument is not supported...");
    }

    public ArgumentNotSupportedException(@NotNull String argument) {
        super("Error! Argument " + argument + " is not supported..");
    }
}
