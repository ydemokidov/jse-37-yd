package com.t1.yd.tm.dto.request.project;

import com.t1.yd.tm.dto.request.AbstractUserRequestByIndex;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProjectChangeStatusByIndexRequest extends AbstractUserRequestByIndex {

    private String status;

}