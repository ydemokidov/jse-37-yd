package com.t1.yd.tm.repository;

import com.t1.yd.tm.api.repository.ITaskRepository;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.model.Task;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @NotNull
    private final String tableName = "tasks";

    public TaskRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final List<Task> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE user_id = ? AND project_id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, projectId);
            @NotNull ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(fetch(resultSet));
            }
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task update(@NotNull final Task task) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET user_id = ?, name = ?, description = ?, status = ?,project_id = ? where id = ?",
                getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getUserId());
            statement.setString(2, task.getName());
            statement.setString(3, task.getDescription());
            statement.setString(4, task.getStatus().getDisplayName());
            statement.setString(5, task.getProjectId());
            statement.setString(6, task.getId());
            statement.executeUpdate();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task add(@NotNull final String userId, @NotNull final Task entity) {
        @NotNull final String sql = String.format("INSERT INTO %s (id,name,description,created,status,user_id) VALUES (?,?,?,?,?,?)", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, entity.getId());
            statement.setString(2, entity.getName());
            statement.setString(3, entity.getDescription());
            statement.setTimestamp(4, new Timestamp(entity.getCreated().getTime()));
            statement.setString(5, entity.getStatus().getDisplayName());
            statement.setString(6, userId);
            statement.executeUpdate();

            return entity;
        }
    }

    @Override
    protected String getTableName() {
        return tableName;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task add(@NotNull final Task entity) {
        @NotNull final String sql = String.format("INSERT INTO %s (id,name,description,created,status) VALUES (?,?,?,?,?)", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, entity.getId());
            statement.setString(2, entity.getName());
            statement.setString(3, entity.getDescription());
            statement.setTimestamp(4, new Timestamp(entity.getCreated().getTime()));
            statement.setString(5, entity.getStatus().getDisplayName());
            statement.executeUpdate();

            return entity;
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    protected Task fetch(@NotNull final ResultSet row) {
        @NotNull final Task task = new Task();
        task.setId(row.getString("id"));
        task.setCreated(row.getTimestamp("created"));
        task.setUserId(row.getString("user_id"));
        task.setName(row.getString("name"));
        task.setDescription(row.getString("description"));
        task.setProjectId(row.getString("project_id"));
        @Nullable final Status status = Status.toStatus(row.getString("status"));
        task.setStatus(status == null ? Status.NOT_STARTED : status);

        return task;
    }

}
