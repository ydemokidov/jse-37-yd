package com.t1.yd.tm.service;

import com.t1.yd.tm.api.repository.ISessionRepository;
import com.t1.yd.tm.api.service.IConnectionService;
import com.t1.yd.tm.api.service.ISessionService;
import com.t1.yd.tm.model.Session;
import com.t1.yd.tm.repository.SessionRepository;
import org.jetbrains.annotations.NotNull;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.sql.Connection;

public class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected ISessionRepository getRepository(@NotNull final Connection connection) {
        return new SessionRepository(connection);
    }

    @Override
    public Connection getConnection() {
        throw new NotImplementedException();
    }

}
