package com.t1.yd.tm.api.service;

import java.sql.Connection;

public interface IConnectionService {

    Connection getConnection();

}
