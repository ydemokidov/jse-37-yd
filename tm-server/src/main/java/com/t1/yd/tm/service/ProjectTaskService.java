package com.t1.yd.tm.service;

import com.t1.yd.tm.api.repository.IProjectRepository;
import com.t1.yd.tm.api.repository.ITaskRepository;
import com.t1.yd.tm.api.service.IConnectionService;
import com.t1.yd.tm.api.service.IProjectTaskService;
import com.t1.yd.tm.exception.entity.ProjectNotFoundException;
import com.t1.yd.tm.exception.entity.TaskNotFoundException;
import com.t1.yd.tm.exception.field.ProjectIdEmptyException;
import com.t1.yd.tm.exception.field.TaskIdEmptyException;
import com.t1.yd.tm.exception.field.UserIdEmptyException;
import com.t1.yd.tm.model.Task;
import com.t1.yd.tm.repository.ProjectRepository;
import com.t1.yd.tm.repository.TaskRepository;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.util.List;

@AllArgsConstructor
public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IConnectionService connectionService;


    @NotNull
    private IProjectRepository getProjectRepository(@NotNull final Connection connection) {
        return new ProjectRepository(connection);
    }

    @NotNull
    private ITaskRepository getTaskRepository(@NotNull final Connection connection) {
        return new TaskRepository(connection);
    }


    @Override
    @SneakyThrows
    public void bindTaskToProject(@NotNull final String userId, @NotNull final String taskId, @NotNull final String projectId) {
        if (projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId.isEmpty()) throw new TaskIdEmptyException();
        if (userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final Connection connection = connectionService.getConnection();

        try {
            @NotNull final IProjectRepository projectRepository = getProjectRepository(connection);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            @NotNull final ITaskRepository taskRepository = getTaskRepository(connection);
            @Nullable final Task task = taskRepository.findOneById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();

            task.setProjectId(projectId);

            taskRepository.update(task);

            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectById(@NotNull final String userId, @NotNull final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();

        @NotNull Connection connection = connectionService.getConnection();
        try {
            @NotNull final IProjectRepository projectRepository = getProjectRepository(connection);
            if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
            @NotNull final ITaskRepository taskRepository = getTaskRepository(connection);
            @NotNull final List<Task> tasks = taskRepository.findAllByProjectId(userId, projectId);
            for (@NotNull final Task task : tasks) {
                taskRepository.removeById(task.getId());
            }
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskFromProject(@NotNull final String userId, @NotNull final String taskId, @NotNull final String projectId) {
        if (projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId.isEmpty()) throw new TaskIdEmptyException();
        if (userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IProjectRepository projectRepository = getProjectRepository(connection);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();

            @NotNull final ITaskRepository taskRepository = getTaskRepository(connection);
            @Nullable final Task task = taskRepository.findOneById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();

            task.setProjectId(null);

            taskRepository.update(task);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
