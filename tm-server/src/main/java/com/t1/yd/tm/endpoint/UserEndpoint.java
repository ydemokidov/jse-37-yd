package com.t1.yd.tm.endpoint;

import com.t1.yd.tm.api.endpoint.IUserEndpoint;
import com.t1.yd.tm.api.service.IServiceLocator;
import com.t1.yd.tm.dto.request.user.*;
import com.t1.yd.tm.dto.response.user.*;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.exception.entity.UserNotFoundException;
import com.t1.yd.tm.exception.field.IdEmptyException;
import com.t1.yd.tm.exception.field.PasswordEmptyException;
import com.t1.yd.tm.exception.user.AccessDeniedException;
import com.t1.yd.tm.model.Session;
import com.t1.yd.tm.model.User;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "com.t1.yd.tm.api.endpoint.IUserEndpoint")
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public @NotNull UserPasswordChangeResponse passwordChange(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final UserPasswordChangeRequest request) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        if (userId == null) throw new AccessDeniedException();
        @Nullable final String newPassword = request.getNewPassword();
        if (newPassword == null) throw new PasswordEmptyException();
        @NotNull final User user = getServiceLocator().getUserService().setPassword(userId, newPassword);
        return new UserPasswordChangeResponse(user);
    }

    @Override
    @WebMethod
    public @NotNull UserLockResponse lock(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final UserLockRequest request) {
        @NotNull final Session session = check(request, Role.ADMIN);
        @Nullable final String userId = session.getUserId();
        if (userId == null) throw new IdEmptyException();
        @Nullable final User user = getServiceLocator().getUserService().findOneById(userId);
        if (user == null) throw new UserNotFoundException();
        getServiceLocator().getUserService().lockByLogin(user.getLogin());
        return new UserLockResponse(user);
    }

    @Override
    @WebMethod
    public @NotNull UserUnlockResponse unlock(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final UserUnlockRequest request) {
        @NotNull final Session session = check(request, Role.ADMIN);
        @Nullable final String userId = session.getUserId();
        if (userId == null) throw new IdEmptyException();
        @Nullable final User user = getServiceLocator().getUserService().findOneById(userId);
        if (user == null) throw new UserNotFoundException();
        getServiceLocator().getUserService().unlockByLogin(user.getLogin());
        return new UserUnlockResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserRegistryResponse registry(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final UserRegistryRequest request) {
        @NotNull final String login = request.getLogin();
        @NotNull final String password = request.getPassword();
        @NotNull final String email = request.getEmail();
        @NotNull final User user = getServiceLocator().getAuthService().registry(login, password, email);

        return new UserRegistryResponse(user);
    }

    @Override
    @WebMethod
    public @NotNull UserRemoveResponse remove(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final UserRemoveRequest request) {
        @NotNull final Session session = check(request, Role.ADMIN);
        @Nullable final String userId = session.getUserId();
        if (userId == null) throw new IdEmptyException();
        @Nullable final User user = getServiceLocator().getUserService().removeById(userId);
        return new UserRemoveResponse(user);
    }

    @Override
    @WebMethod
    public @NotNull UserUpdateProfileResponse updateProfile(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final UserUpdateProfileRequest request) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        if (userId == null) throw new IdEmptyException();
        @Nullable final String lastName = request.getLastName();
        @Nullable final String firstName = request.getFirstName();
        @Nullable final String middleName = request.getMidName();
        @Nullable final User user = getServiceLocator().getUserService().updateUser(userId, firstName, lastName, middleName);
        return new UserUpdateProfileResponse(user);
    }

}
