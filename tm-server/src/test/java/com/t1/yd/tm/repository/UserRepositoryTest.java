package com.t1.yd.tm.repository;

import com.t1.yd.tm.api.repository.IUserRepository;
import com.t1.yd.tm.api.service.IConnectionService;
import com.t1.yd.tm.api.service.IPropertyService;
import com.t1.yd.tm.marker.UnitCategory;
import com.t1.yd.tm.service.ConnectionService;
import com.t1.yd.tm.service.PropertyService;
import lombok.SneakyThrows;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static com.t1.yd.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public class UserRepositoryTest {

    private IUserRepository repository;

    private IPropertyService propertyService = new PropertyService();

    private IConnectionService connectionService = new ConnectionService(propertyService);

    @Before
    public void initRepository() {
        repository = new UserRepository(connectionService.getConnection());
        Assert.assertTrue(repository.findAll().isEmpty());
    }

    @Test
    public void add() {
        repository.add(USER1);
        Assert.assertEquals(USER1.getId(), repository.findOneById(USER1.getId()).getId());
    }

    @Test
    public void addAll() {
        repository.add(ALL_USERS);
        Assert.assertEquals(ALL_USERS.size(), repository.findAll().size());
        Assert.assertEquals(USER1.getId(), repository.findOneById(USER1.getId()).getId());
    }


    @Test
    public void clear() {
        repository.add(ALL_USERS);
        repository.clear();
        Assert.assertTrue(repository.findAll().isEmpty());
    }

    @Test
    public void findByLogin() {
        repository.add(USER1);
        repository.add(ADMIN);
        Assert.assertEquals(USER1.getId(), repository.findByLogin(USER1_LOGIN).getId());
    }

    @Test
    public void findByEmail() {
        repository.add(USER1);
        Assert.assertEquals(USER1.getId(), repository.findByEmail(USER1_EMAIL).getId());
    }

    @Test
    public void existsByLogin() {
        repository.add(USER1);
        Assert.assertTrue(repository.isLoginExist(USER1_LOGIN));
        Assert.assertFalse(repository.isLoginExist(ADMIN_LOGIN));
    }

    @Test
    public void existsByEmail() {
        repository.add(USER1);
        Assert.assertTrue(repository.isEmailExist(USER1_EMAIL));
        Assert.assertFalse(repository.isEmailExist(ADMIN_EMAIL));
    }

    @After
    @SneakyThrows
    public void clearData() {
        repository.clear();
        repository.getConnection().close();
    }

}
