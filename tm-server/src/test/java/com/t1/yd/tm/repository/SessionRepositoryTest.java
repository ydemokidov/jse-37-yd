package com.t1.yd.tm.repository;

import com.t1.yd.tm.api.repository.ISessionRepository;
import com.t1.yd.tm.api.service.IConnectionService;
import com.t1.yd.tm.api.service.IPropertyService;
import com.t1.yd.tm.model.Session;
import com.t1.yd.tm.service.ConnectionService;
import com.t1.yd.tm.service.PropertyService;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static com.t1.yd.tm.constant.SessionTestData.*;
import static com.t1.yd.tm.constant.UserTestData.ADMIN;
import static com.t1.yd.tm.constant.UserTestData.USER1;

public class SessionRepositoryTest {

    private IPropertyService propertyService = new PropertyService();

    private IConnectionService connectionService = new ConnectionService(propertyService);

    private ISessionRepository repository;

    @Before
    public void initRepository() {
        repository = new SessionRepository(connectionService.getConnection());
        Assert.assertTrue(repository.findAll().isEmpty());
    }

    @Test
    public void add() {
        repository.add(SESSION1);
        Assert.assertEquals(SESSION1.getId(), repository.findOneById(SESSION1.getId()).getId());
    }

    @Test
    public void addWithUser() {
        repository.add(USER1.getId(), SESSION1);
        Assert.assertEquals(SESSION1.getId(), repository.findAll(USER1.getId()).get(0).getId());
    }

    @Test
    public void addAll() {
        repository.add(ALL_SESSIONS);
        Assert.assertEquals(ALL_SESSIONS.size(), repository.findAll().size());
        Assert.assertEquals(SESSION1.getId(), repository.findAll().get(0).getId());
    }

    @Test
    public void clear() {
        repository.add(ALL_SESSIONS);
        repository.clear();
        Assert.assertTrue(repository.findAll().isEmpty());
    }

    @Test
    public void findAllByUserId() {
        repository.add(USER1.getId(), SESSION1);
        repository.add(ADMIN.getId(), SESSION2);
        Assert.assertEquals(1, repository.findAll(USER1.getId()).size());
    }

    @Test
    public void existsById() {
        repository.add(SESSION1);
        Assert.assertTrue(repository.existsById(SESSION1.getId()));
        Assert.assertFalse(repository.existsById(SESSION2.getId()));
    }

    @Test
    public void removeById() {
        repository.add(ALL_SESSIONS);
        repository.removeById(SESSION1.getId());
        Assert.assertEquals(ALL_SESSIONS.size() - 1, repository.findAll().size());
        Assert.assertNull(repository.findOneById(SESSION1.getId()));
    }

    @Test
    public void removeByIndex() {
        repository.add(USER1.getId(), SESSION1);
        repository.add(ADMIN.getId(), SESSION2);
        @Nullable final Session removedSession = repository.removeByIndex(1);
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(ALL_SESSIONS.size() - 1, repository.findAll().size());
        Assert.assertNull(repository.findOneById(removedSession.getId()));
    }


    @Test
    public void findOneByIdWithUserId() {
        repository.add(USER1.getId(), SESSION1);
        repository.add(ADMIN.getId(), SESSION2);

        Assert.assertEquals(SESSION1.getId(), repository.findOneById(USER1.getId(), SESSION1.getId()).getId());
        Assert.assertNull(repository.findOneById(ADMIN.getId(), SESSION1.getId()));
    }

    @After
    @SneakyThrows
    public void clearData() {
        repository.clear();
        repository.getConnection().close();
    }

}
