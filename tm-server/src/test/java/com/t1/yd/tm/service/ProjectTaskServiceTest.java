package com.t1.yd.tm.service;

import com.t1.yd.tm.api.service.*;
import com.t1.yd.tm.marker.UnitCategory;
import com.t1.yd.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static com.t1.yd.tm.constant.ProjectTestData.USER1_PROJECT1;
import static com.t1.yd.tm.constant.TaskTestData.USER1_PROJECT1_TASK1;
import static com.t1.yd.tm.constant.UserTestData.USER1;

@Category(UnitCategory.class)
public class ProjectTaskServiceTest {

    private IPropertyService propertyService = new PropertyService();

    private IConnectionService connectionService = new ConnectionService(propertyService);

    private IProjectService projectService;

    private ITaskService taskService;

    private IProjectTaskService service;

    @Before
    public void initRepository() {
        service = new ProjectTaskService(connectionService);
        projectService = new ProjectService(connectionService);
        taskService = new TaskService(connectionService);
    }

    @Test
    public void bindTaskToProject() {
        projectService.add(USER1.getId(), USER1_PROJECT1);
        taskService.add(USER1.getId(), USER1_PROJECT1_TASK1);
        service.bindTaskToProject(USER1.getId(), USER1_PROJECT1_TASK1.getId(), USER1_PROJECT1.getId());
        @NotNull final Task bindedTask = taskService.findTaskById(USER1.getId(), USER1_PROJECT1_TASK1.getId());
        Assert.assertEquals(USER1_PROJECT1.getId(), bindedTask.getProjectId());
    }

    @Test
    public void unbindTaskFromProject() {
        projectService.add(USER1.getId(), USER1_PROJECT1);
        taskService.add(USER1.getId(), USER1_PROJECT1_TASK1);
        service.bindTaskToProject(USER1.getId(), USER1_PROJECT1_TASK1.getId(), USER1_PROJECT1.getId());
        service.unbindTaskFromProject(USER1.getId(), USER1_PROJECT1_TASK1.getId(), USER1_PROJECT1.getId());
        Assert.assertTrue(taskService.findAllByProjectId(USER1.getId(), USER1_PROJECT1.getId()).isEmpty());
    }

    @Test
    public void removeProjectById() {
        projectService.add(USER1.getId(), USER1_PROJECT1);
        taskService.add(USER1.getId(), USER1_PROJECT1_TASK1);
        service.bindTaskToProject(USER1.getId(), USER1_PROJECT1_TASK1.getId(), USER1_PROJECT1.getId());
        service.removeProjectById(USER1.getId(), USER1_PROJECT1.getId());
        Assert.assertTrue(taskService.findAllByProjectId(USER1.getId(), USER1_PROJECT1.getId()).isEmpty());
    }

    @After
    public void clearData() {
        taskService.clear();
        projectService.clear();
    }

}