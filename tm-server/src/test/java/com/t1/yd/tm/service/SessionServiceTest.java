package com.t1.yd.tm.service;

import com.t1.yd.tm.api.service.IConnectionService;
import com.t1.yd.tm.api.service.IPropertyService;
import com.t1.yd.tm.api.service.ISessionService;
import com.t1.yd.tm.model.Session;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static com.t1.yd.tm.constant.SessionTestData.*;
import static com.t1.yd.tm.constant.UserTestData.ADMIN;
import static com.t1.yd.tm.constant.UserTestData.USER1;

public class SessionServiceTest {

    private IPropertyService propertyService = new PropertyService();

    private IConnectionService connectionService = new ConnectionService(propertyService);

    private ISessionService service = new SessionService(connectionService);

    @Before
    public void initRepository() {
        service = new SessionService(connectionService);
    }

    @Test
    public void add() {
        service.add(SESSION1);
        Assert.assertEquals(SESSION1.getId(), service.findOneById(SESSION1.getId()).getId());
    }

    @Test
    public void addWithUser() {
        service.add(USER1.getId(), SESSION1);
        Assert.assertEquals(SESSION1.getId(), service.findAll(USER1.getId()).get(0).getId());
    }

    @Test
    public void addAll() {
        service.add(ALL_SESSIONS);
        Assert.assertEquals(ALL_SESSIONS.size(), service.findAll().size());
        Assert.assertEquals(SESSION1.getId(), service.findAll().get(0).getId());
    }

    @Test
    public void clear() {
        service.add(ALL_SESSIONS);
        service.clear();
        Assert.assertTrue(service.findAll().isEmpty());
    }

    @Test
    public void findAllByUserId() {
        service.add(USER1.getId(), SESSION1);
        service.add(ADMIN.getId(), SESSION2);
        Assert.assertEquals(1, service.findAll(USER1.getId()).size());
    }

    @Test
    public void existsById() {
        service.add(SESSION1);
        Assert.assertTrue(service.existsById(SESSION1.getId()));
        Assert.assertFalse(service.existsById(SESSION2.getId()));
    }

    @Test
    public void removeById() {
        service.add(ALL_SESSIONS);
        service.removeById(SESSION1.getId());
        Assert.assertEquals(ALL_SESSIONS.size() - 1, service.findAll().size());
        Assert.assertNull(service.findOneById(SESSION1.getId()));
    }

    @Test
    public void removeByIndex() {
        service.add(USER1.getId(), SESSION1);
        service.add(ADMIN.getId(), SESSION2);
        @Nullable final Session removedSession = service.removeByIndex(1);
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(ALL_SESSIONS.size() - 1, service.findAll().size());
        Assert.assertNull(service.findOneById(removedSession.getId()));
    }


    @Test
    public void findOneByIdWithUserId() {
        service.add(USER1.getId(), SESSION1);
        service.add(ADMIN.getId(), SESSION2);

        Assert.assertEquals(SESSION1.getId(), service.findOneById(USER1.getId(), SESSION1.getId()).getId());
        Assert.assertNull(service.findOneById(ADMIN.getId(), SESSION1.getId()));
    }

    @After
    public void clearData() {
        service.clear();
    }

}
