package com.t1.yd.tm.service;

import com.t1.yd.tm.api.service.IConnectionService;
import com.t1.yd.tm.api.service.IPropertyService;
import com.t1.yd.tm.api.service.ITaskService;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.model.Task;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static com.t1.yd.tm.constant.ProjectTestData.ALL_PROJECTS;
import static com.t1.yd.tm.constant.TaskTestData.*;
import static com.t1.yd.tm.constant.UserTestData.ADMIN;
import static com.t1.yd.tm.constant.UserTestData.USER1;

public class TaskServiceTest {

    private IPropertyService propertyService = new PropertyService();

    private IConnectionService connectionService = new ConnectionService(propertyService);

    private ITaskService service;

    @Before
    public void initRepository() {
        service = new TaskService(connectionService);
    }

    @Test
    public void add() {
        service.add(USER1_PROJECT1_TASK1);
        Assert.assertEquals(USER1_PROJECT1_TASK1.getId(), service.findOneById(USER1_PROJECT1_TASK1.getId()).getId());
    }

    @Test
    public void addWithUser() {
        service.add(USER1.getId(), USER1_PROJECT1_TASK1);
        Assert.assertEquals(USER1_PROJECT1_TASK1.getId(), service.findOneById(USER1_PROJECT1_TASK1.getId()).getId());
    }

    @Test
    public void addAll() {
        service.add(ALL_TASKS);
        Assert.assertEquals(ALL_TASKS.size(), service.findAll().size());
        Assert.assertEquals(USER1_PROJECT1_TASK1.getId(), service.findOneById(USER1_PROJECT1_TASK1.getId()).getId());
    }

    @Test
    public void removeById() {
        service.add(ALL_TASKS);
        service.removeById(USER1_PROJECT1_TASK2.getId());
        Assert.assertEquals(ALL_PROJECTS.size() - 1, service.findAll().size());
        Assert.assertNull(service.findOneById(USER1_PROJECT1_TASK2.getId()));
    }

    @Test
    public void removeByIndex() {
        service.add(ALL_TASKS);
        @Nullable final Task removedTask = service.removeByIndex(1);
        Assert.assertNotNull(removedTask);
        Assert.assertEquals(ALL_TASKS.size() - 1, service.findAll().size());
        Assert.assertNotEquals(removedTask.getId(), service.findOneByIndex(1).getId());
    }

    @Test
    public void clear() {
        service.add(ALL_TASKS);
        service.clear();
        Assert.assertTrue(service.findAll().isEmpty());
    }

    @Test
    public void findAllByUserId() {
        service.add(USER1.getId(), USER1_PROJECT1_TASK1);
        service.add(USER1.getId(), USER1_PROJECT1_TASK2);
        service.add(ADMIN.getId(), ADMIN_PROJECT1_TASK1);
        Assert.assertEquals(USER1_PROJECT1_TASKS.size(), service.findAll(USER1.getId()).size());
    }

    @Test
    public void findOneByIdWithUserId() {
        service.add(USER1.getId(), USER1_PROJECT1_TASK1);
        service.add(USER1.getId(), USER1_PROJECT1_TASK2);
        service.add(ADMIN.getId(), ADMIN_PROJECT1_TASK1);

        Assert.assertEquals(USER1_PROJECT1_TASK1.getId(), service.findOneById(USER1.getId(), USER1_PROJECT1_TASK1.getId()).getId());
        Assert.assertNull(service.findOneById(ADMIN.getId(), USER1_PROJECT1_TASK1.getId()));
    }

    @Test
    public void findOneByIndexWithUserIdPositive() {
        service.add(USER1.getId(), USER1_PROJECT1_TASK1);
        Assert.assertEquals(USER1_PROJECT1_TASK1.getId(), service.findOneByIndex(USER1.getId(), 1).getId());
    }

    @Test
    public void findOneByIndexWithUserIdNegative() {
        service.add(USER1.getId(), USER1_PROJECT1_TASK1);
        Assert.assertNull(service.findOneByIndex(ADMIN.getId(), 1));
    }

    @Test
    public void existsById() {
        service.add(USER1_PROJECT1_TASK1);
        Assert.assertTrue(service.existsById(USER1_PROJECT1_TASK1.getId()));
        Assert.assertFalse(service.existsById(USER1_PROJECT1_TASK2.getId()));
    }

    @Test
    public void updateById() {
        service.add(USER1.getId(), USER1_PROJECT1_TASK1);
        service.updateById(USER1.getId(), USER1_PROJECT1_TASK1.getId(), TASK_NAME, TASK_DESCRIPTION);
        Assert.assertEquals(TASK_NAME, service.findTaskById(USER1.getId(), USER1_PROJECT1_TASK1.getId()).getName());
        Assert.assertEquals(TASK_DESCRIPTION, service.findTaskById(USER1.getId(), USER1_PROJECT1_TASK1.getId()).getDescription());
    }

    @Test
    public void updateByIndex() {
        service.add(USER1.getId(), USER1_PROJECT1_TASK1);
        service.updateByIndex(USER1.getId(), 1, TASK_NAME, TASK_DESCRIPTION);
        Assert.assertEquals(TASK_NAME, service.findTaskById(USER1.getId(), USER1_PROJECT1_TASK1.getId()).getName());
        Assert.assertEquals(TASK_DESCRIPTION, service.findTaskById(USER1.getId(), USER1_PROJECT1_TASK1.getId()).getDescription());
    }

    @Test
    public void changeStatusById() {
        service.add(USER1.getId(), USER1_PROJECT1_TASK1);
        service.changeStatusById(USER1.getId(), USER1_PROJECT1_TASK1.getId(), Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, service.findTaskById(USER1.getId(), USER1_PROJECT1_TASK1.getId()).getStatus());
    }

    @Test
    public void changeStatusByIndex() {
        service.add(USER1.getId(), USER1_PROJECT1_TASK1);
        service.changeStatusByIndex(USER1.getId(), 1, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, service.findTaskById(USER1.getId(), USER1_PROJECT1_TASK1.getId()).getStatus());
    }

    @After
    public void clearData() {
        service.clear();
    }

}
